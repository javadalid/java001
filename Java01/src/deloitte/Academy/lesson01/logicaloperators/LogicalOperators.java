package deloitte.Academy.lesson01.logicaloperators;

import java.util.logging.Level;

import java.util.logging.Logger;

import deloitte.Academy.lesson01.comparisons.Comparisons;

/**
 * 
 * @author Javier Adalid Schmid 
 * Clase de operadores lógicos (Uso de AND, OR, NOT, etc.)
 *
 */
public class LogicalOperators {

	private static final Logger LOGGER= Logger.getLogger(Comparisons.class.getName());

	
	
	/**
	 *  Method that compares if two numbers are pairs.
	 * @param valor1: first parameter is a number (integer)
	 * @param valor2: second parameter is a number (integer)
	 * @return res: result as a boolean, true or false
	 */
	public static boolean sonPares(int valor1, int valor2) {
		boolean res= false;
		try {
			if (valor1%2==0 && valor2%2==0) {
				res=true;
			}
			LOGGER.info("Comparación de pares satisfactoria con resultado: " + res);
		}
		catch(Exception ex){
			LOGGER.log(Level.SEVERE, "Error al realizar la comparación de pares. ", ex);
		}
		
		return res;
	}
	
	
	/**
	 *   Method that compares if at least 1 out of 2 numbers, is pair.
	 * @param valor1: first parameter is a number (integer)
	 * @param valor2: second parameter is a number (integer)
	 * @return res: result as a boolean, true or false
	 */
	public static boolean unoEsPar(int valor1, int valor2) {
		boolean res= false;
		try {
			if (valor1%2==0 || valor2%2==0) {
				res= true;
			}
			LOGGER.info("Comparación de al menos un par satisfactoria con resultado: " + res);
		}
		catch(Exception ex){
			LOGGER.log(Level.SEVERE, "Error al realizar la comparación de al menos un par. ", ex);
		}
		
		return res;
	}
	

	/**
	 * Method that compares if one number is pair and the other is not.
	 * @param valor1: first parameter is a number (integer)
	 * @param valor2: second parameter is a number (integer)
	 * @return res: result as a boolean, true or false
	 */
	public static boolean parImpar(int valor1, int valor2) {
		boolean res= false;
		try {
			if ((valor1%2==0 && valor2%2!=0) || (valor1%2!=0 && valor2%2==0)  ) {
				res= true;
			}
			LOGGER.info("Comparación de par e impar satisfactoria con resultado: " + res);
		}
		catch(Exception ex){
			LOGGER.log(Level.SEVERE, "Error al realizar la comparación de par e impar. ", ex);
		}
		
		return res;
	}
}
