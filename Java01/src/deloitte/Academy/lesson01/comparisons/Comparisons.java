package deloitte.Academy.lesson01.comparisons;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase de comparadores, se utiliza el igual a, diferente a, mayor que,
 * menor que, etc.
 * @author Javier Adalid Schmid 
 *
 */
public class Comparisons {
	private static final Logger LOGGER = Logger.getLogger(Comparisons.class.getName());

	/**
	 * Compares if two strings are exactly the same.
	 * 
	 * @param valor1: first parameter is a string
	 * @param valor2: second parameter is also a string
	 * @return res: result as a boolean, tells if it is true or false.
	 */
	public static boolean textoIgual(String valor1, String valor2) {
		boolean res = false;
		try {
			valor1 = valor1.toUpperCase();
			valor2 = valor2.toUpperCase();
			if (valor1 == valor2) {
				res = true;
			}
			LOGGER.info("Comparación de a=b realizada exitosamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error al realizar la comparación de a=b. ", ex);
		}

		return res;
	}

	/**
	 * Compares if "valor1" is greater or equal than "valor2". 
	 * 
	 * @param valor1: first parameter is a number, type double
	 * @param valor2: second parameter is also a number type double
	 * @return res: result as a boolean, tells if it is true or false
	 */
	public static boolean numeroMayorIgual(double valor1, double valor2) {
		boolean res = false;
		try {
			if (valor1 >= valor2) {
				res = true;
			}
			LOGGER.info("Comparación de a mayor-igual a b realizada exitosamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error al realizar la comparación a mayor-igual b. ", ex);
		}

		return res;
	}

	/**
	 * Compares if "valor1" is different than "valor2". 
	 * 
	 * @param valor1: first parameter is a number, type double
	 * @param valor2: second parameter is also a number, type double
	 * @return res: result as a boolean, tells if it is true or false
	 */
	public static boolean numeroDiferente(double valor1, double valor2) {
		boolean res = false;
		try {
			if (valor1 != valor2) {
				res = true;
			}
			LOGGER.info("Comparación de número diferente realizada exitosamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Error al realizar la comparación numero diferente. ", ex);
		}

		return res;
	}

}
