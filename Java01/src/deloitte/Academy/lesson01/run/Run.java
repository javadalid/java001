package deloitte.Academy.lesson01.run;
import java.io.IOException;

import deloitte.Academy.lesson01.aritmethic.Aritmethic;
import deloitte.Academy.lesson01.comparisons.Comparisons;
import deloitte.Academy.lesson01.logicaloperators.LogicalOperators;

/**
 * 
 * @author Javier Adalid Schmid
 *
 * Clase main en donde se implementan todos los m�todos de las clases:
 * Aritmethic, Comparisons, LogicalOperators.
 */

public class Run {

	public static void main(String[] args) throws IOException, SecurityException {
		Aritmethic.suma(9, 10);
		Aritmethic.resta(9, 3);
		Aritmethic.multiplicacion(9,7);
		Aritmethic.division(9, 0);
		Aritmethic.modulo(8, 3.8);
		Comparisons.numeroDiferente(3, 3);
		Comparisons.numeroMayorIgual(5, 2);
		Comparisons.textoIgual("hola", "HOLA");
		LogicalOperators.parImpar(2, 5);
		LogicalOperators.sonPares(3, 7);
		LogicalOperators.unoEsPar(5, 10);

	}

}
