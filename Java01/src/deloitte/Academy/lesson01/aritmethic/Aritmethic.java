package deloitte.Academy.lesson01.aritmethic;
import java.util.logging.Level;
import java.io.IOException;

import java.util.logging.Logger;

/**
 * Clase de operadores aritm�ticos. Realiza sumas,restas
 * multiplicaciones, divisiones y m�dulo
 * @author Javier Adalid Schmid 
 *
 */

// Comentario a�adido
public class Aritmethic {
	private static final Logger LOGGER = Logger.getLogger(Aritmethic.class.getName());

	public static double res = 0;

	/**
	 * Makes an addition with two numbers provided
	 * @param valor1 : the first parameter is a number, type double
	 * @param valor2 : the second parameter is also a number, type double
	 * @return res: the result is a number, type double.
	 */
	public static double suma(double valor1, double valor2) {
		try {
			res = valor1 + valor2;
			LOGGER.info("Suma realizada correctamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "No se pudo realizar la suma", ex);
		}
		return res;
	}

	
	/**
	 * Makes a subtraction with two numbers provided
	 * @param valor1 : the first parameter is a number, type double
	 * @param valor2 : the second parameter is also a number, type double
	 * @return res: the result is a number, type double.
	 */
	public static double resta(double valor1, double valor2) {
		try {
			res = valor1 - valor2;
			LOGGER.info("Resta realizada correctamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "No se pudo realizar la resta", ex);
		}
		return res;
	}

	
	/**
	 * Makes a multiplication between two numbers provided
	 * @param valor1 : the first parameter is a number, type double
	 * @param valor2 : the second parameter is also a number, type double
	 * @return res: the result is a number, type double.
	 */
	public static double multiplicacion(double valor1, double valor2) {
		try {
			res = valor1 * valor2;
			LOGGER.info("Multiplicacion realizada correctamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "No se pudo realizar la multiplicacion", ex);
		}
		return res;
	}

	/**
	 * Makes a division
	 * @param valor1 : the first parameter is a number, type double
	 * @param valor2 : the second parameter is also a number, type double
	 * @return res: the result is a number, type double.
	 */
	public static double division(double valor1, double valor2) {
		if (valor2 == 0) {
			LOGGER.log(Level.SEVERE, "Division entre 0");
			return 0;
		}
		try {
			res = valor1 / valor2;
			LOGGER.info("Divisi�n realizada correctamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "No se pudo realizar la division", ex);
		}
		return res;
	}

	
	/**
	 * Get the module with two parameters, the result is the quotient.
	 * @param valor1 : the first parameter is a number, type double
	 * @param valor2 : the second parameter is also a number, type double
	 * @return res: the result is a number, type double.
	 */
	public static double modulo(double valor1, double valor2) {
		try {
			res = valor1 % valor2;
			LOGGER.info("Modulo realizado correctamente con resultado: " + res);
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "No se pudo realizar el modulo", ex);
		}
		return res;
	}

}
